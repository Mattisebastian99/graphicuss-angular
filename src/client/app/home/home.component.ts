import { Component, OnInit } from '@angular/core';
import { NameListService } from '../shared/name-list/name-list.service';

import DrawingFabric from '../../assets/drawing-fabric.js';

// declare var DrawingFabric: any;
declare var fabric: any;
declare var $: any;


/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
})
export class HomeComponent implements OnInit {


  canvas: any = null;
  newName: string = '';
  errorMessage: string;
  names: any[] = [];

  /**
   * Creates an instance of the HomeComponent with the injected
   * NameListService.
   *
   * @param {NameListService} nameListService - The injected NameListService.
   */
  constructor(public nameListService: NameListService) {}

  /**
   * Get the names OnInit
   */
  ngOnInit() {

 //    let _self = this;
 // $(function(){
    var c: any = new DrawingFabric.Canvas('drawingCanvas');
    console.log();
     c.addFunctionality(new DrawingFabric.Functionality.tools({
        cursor:    $('.js-tools-cursor'),
        ellipse:   $('.js-tools-ellipse'),
        rectangle: $('.js-tools-rectangle'),
        triangle:  $('.js-tools-triangle'),
        line:      $('.js-tools-line'),
        draw:      $('.js-tools-draw'),
        arc:       $('.js-tools-arc'),
        text:      $('.js-tools-text')
      }));
      c.addFunctionality(new DrawingFabric.Functionality.addDoubleClick());
      c.addFunctionality(new DrawingFabric.Functionality.addText());
      c.addFunctionality(new DrawingFabric.Functionality.drawWithMouse());
      c.addFunctionality(new DrawingFabric.Functionality.selectWithCursor());
      c.addFunctionality(new DrawingFabric.Functionality.drawArcWithMouse());
      c.addFunctionality(new DrawingFabric.Functionality.drawShapeWithMouse());
      c.addFunctionality(new DrawingFabric.Functionality.drawLineWithMouse());
      c.addFunctionality(new DrawingFabric.Functionality.selectedProperties({
        strokeWidth: {
          value:  $('.js-selected-properties-stroke-width-value'),
          parent: $('.js-selected-properties-stroke-width')
        },
        stroke: {
          value:  $('.js-selected-properties-stroke-value'),
          parent: $('.js-selected-properties-stroke')
        },
        fill: {
          value: $ ('.js-selected-properties-fill-value'),
          parent: $('.js-selected-properties-fill')
        },
      }));

//// Use spectrum colour picker
      $('.js-color').each(function(i,e){
        var $e = $(e);

        // Enable spectrum on each input
        $e.spectrum({
          showAlpha:       true,
          preferredFormat: 'rgb'
        });

        // Ensure if the input changes spectrum is updated
        $e.change(function(){
          var newVal = $e.val();
          if($e.spectrum('get') != newVal){
            $e.spectrum('set',newVal);
          }
        });
      });

  //     _self.canvas = c;
  // });
}

}



