import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { HttpModule } from '@angular/http';
import { InMemoryDataService }  from './in-memory-data.service';

import { AboutModule } from './about/about.module';
import { HomeModule } from './home/home.module';

import { CourseService }         from './courses/course.service';
import { CoursesComponent }         from './courses/courses.component';
import { CoursesModule }         from './courses/courses.module';

@NgModule({
  imports: [BrowserModule, HttpModule, InMemoryWebApiModule.forRoot(InMemoryDataService), AppRoutingModule, AboutModule, HomeModule, CoursesModule],
  declarations: [AppComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  },
  CourseService],
  bootstrap: [AppComponent]

})
export class AppModule { }
