import { Component } from '@angular/core';
 
@Component({
  selector: 'my-app',
  template: `
    <h1>{{title}}</h1>
    <nav>
     <a routerLink="/courses">Courses</a>
     <a routerLink="/">Draw!</a>
   </nav>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {
  title = 'Courses';
}