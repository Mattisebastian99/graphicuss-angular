import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './courses.component';
import { CoursesRoutingModule } from './courses-routing.module';

import { FormsModule }   from '@angular/forms'; // <-- NgModel lives here

@NgModule({
  imports: [CommonModule, CoursesRoutingModule, FormsModule],
  declarations: [CoursesComponent],
  exports: [CoursesComponent]
})
export class CoursesModule { }
