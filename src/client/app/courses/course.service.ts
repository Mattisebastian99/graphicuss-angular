import { Injectable }    from '@angular/core';
import { Http, Headers } from '@angular/http';


import { Course } from './course';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class CourseService {
  private courseUrl = 'api/courses';  // URL to web api
  private headers = new Headers({'Content-Type': 'application/json'});
 
constructor(private http: Http) { }
 
getCourse(): Promise<Course> {
  const url = '${this.courseUrl}/${id}'
  return this.http.get(url)
             .toPromise()
             .then(response => response.json() as Course)
             .catch(this.handleError);
}

getCourses(): Promise<Course[]> {
  return this.http.get(this.courseUrl)
             .toPromise()
             .then(response => response.json() as Course[])
             .catch(this.handleError);
}

update(course: Course): Promise<Course> {
  const url = `${this.courseUrl}/${course.id}`;
  return this.http
    .put(url, JSON.stringify(course), {headers: this.headers})
    .toPromise()
    .then(() => course)
    .catch(this.handleError);
}

create(name: string): Promise<Course> {
  return this.http
    .post(this.courseUrl, JSON.stringify({name: name}), {headers: this.headers})
    .toPromise()
    .then(res => res.json() as Course)
    .catch(this.handleError);
}


 
private handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
}
}