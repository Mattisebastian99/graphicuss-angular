import { Component, OnInit } from '@angular/core';
import { Config } from './../shared/config/env.config';
import './../operators';

import { CourseService } from './course.service';
import { Course } from './course';

/**
 * This class represents the main application component.
 */
@Component({
  moduleId: module.id,
  selector: 'my-courses',
  templateUrl: 'courses.component.html',
  styleUrls: ['courses.component.css'],
})
export class CoursesComponent implements OnInit {

  courses: Course[];
  selectedCourse: Course;

  constructor(private courseService: CourseService) {
   
  }

  getCourses(): void {
    this.courseService.getCourses().then(courses => this.courses = courses);
  }
 
  ngOnInit(): void {
    this.getCourses();
  }
 
  onSelect(course: Course): void {
    this.selectedCourse = course;
  }


  save(): void {
    this.courseService.update(this.selectedCourse);
    //.then(() => this.goBack());
}

add(name: string): void {
  name = name.trim();
  if (!name) { return; }
  this.courseService.create(name)
    .then(course => {
      this.courses.push(course);
      this.selectedCourse = null;
    });
}

}
